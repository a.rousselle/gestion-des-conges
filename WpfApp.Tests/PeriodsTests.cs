﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using WpfApp.Tests.TestImplementations;
using WpfApp.UI;

namespace WpfApp.Tests
{
	[TestClass]
	public class PeriodsTests
	{
		[TestMethod]
		public void Test1()
		{
			RunTest(new DateTime(2018, 9, 1), new[]
			{
				new TestPeriod(new DateTime(2018, 12, 25), new DateTime(2019, 1, 15), 21, 0, 0, 21),
				new TestPeriod(new DateTime(2019, 1, 16), new DateTime(2019, 1, 25), 10, 0, 0, 10),
				new TestPeriod(new DateTime(2019, 4, 1), new DateTime(2019, 4, 3), 3, 3, 0, 0),
				new TestPeriod(new DateTime(2019, 4, 4), new DateTime(2019, 4, 30), 27, 27, 0, 0),
				new TestPeriod(new DateTime(2019, 5, 1), new DateTime(2019, 5, 4), 4, 0, 4, 0),
				new TestPeriod(new DateTime(2019, 5, 15), new DateTime(2019, 6, 10), 26, 0, 26, 0),
				new TestPeriod(new DateTime(2019, 6, 11), new DateTime(2019, 6, 15), 5, 0, 0, 5),
			});
		}

		[TestMethod]
		public void Test2()
		{
			RunTest(new DateTime(2018, 9, 1), new[]
			{
				new TestPeriod(new DateTime(2018, 12, 25), new DateTime(2019, 1, 15), 21, 0, 0, 21),
				new TestPeriod(new DateTime(2019, 1, 16), new DateTime(2019, 1, 25), 10, 0, 0, 10),
				new TestPeriod(new DateTime(2019, 1, 26), new DateTime(2019, 6, 30), 155, 0, 0, 155),
			});
		}

		[TestMethod]
		public void Test3()
		{
			RunTest(new DateTime(2018, 9, 1), new[]
			{
				new TestPeriod(new DateTime(2018, 11, 1), new DateTime(2018, 11, 30), 30, 0, 0, 30),
				new TestPeriod(new DateTime(2019, 6, 1), new DateTime(2019, 6, 30), 30, 30, 0, 0),
				new TestPeriod(new DateTime(2019, 8, 1), new DateTime(2019, 8, 5), 5, 0, 5, 0),
			});
		}

		[TestMethod]
		public void Test4()
		{
			RunTest(new DateTime(2018, 9, 1), new[]
			{
				new TestPeriod(new DateTime(2019, 2, 1), new DateTime(2019, 2, 28), 30, 30, 0, 0),
				new TestPeriod(new DateTime(2019, 5, 3), new DateTime(2019, 5, 10), 8, 0, 8, 0),
			});
		}

		[TestMethod]
		public void Test5()
		{
			RunTest(new DateTime(2018, 9, 1), new[]
			{
				new TestPeriod(new DateTime(2018, 10, 1), new DateTime(2018, 10, 5), 5, 0, 0, 5),
				new TestPeriod(new DateTime(2019, 1, 5), new DateTime(2019, 1, 10), 6, 6, 0, 0),
			});
		}

		[TestMethod]
		public void Test6()
		{
			RunTest(new DateTime(2018, 9, 1), new[]
			{
				new TestPeriod(new DateTime(2019, 1, 31), new DateTime(2019, 1, 31), 0, 0, 0, 0),
			});
		}

		[TestMethod]
		public void Test7()
		{
			RunTest(new DateTime(2017, 9, 1), new[]
			{
				new TestPeriod(new DateTime(2019, 2, 2), new DateTime(2019, 2, 28), 29, 29, 0, 0),
			});
		}

		[TestMethod]
		public void Test8()
		{
			RunTest(new DateTime(2017, 9, 1), new[]
			{
				new TestPeriod(new DateTime(2018, 2, 2), new DateTime(2018, 3, 1), 30, 30, 0, 0),
				new TestPeriod(new DateTime(2018, 3, 2), new DateTime(2018, 3, 5), 4, 0, 4, 0),
			});
		}

		[TestMethod]
		public void Test9()
		{
			RunTest(new DateTime(2016, 9, 1), new[]
			{
				new TestPeriod(new DateTime(2018, 6, 1), new DateTime(2018, 6, 30), 30, 30, 0, 0),
				new TestPeriod(new DateTime(2018, 10, 1), new DateTime(2018, 10, 31), 30, 30, 0, 0),
				new TestPeriod(new DateTime(2018, 11, 1), new DateTime(2018, 11, 30), 30, 0, 30, 0),
			});
		}

		[TestMethod]
		public void Test10()
		{
			RunTest(new DateTime(2016, 9, 1), new[]
			{
				new TestPeriod(new DateTime(2018, 6, 1), new DateTime(2018, 6, 30), 30, 30, 0, 0),
				new TestPeriod(new DateTime(2018, 7, 1), new DateTime(2018, 7, 31), 30, 0, 30, 0),
				new TestPeriod(new DateTime(2018, 10, 1), new DateTime(2018, 10, 31), 30, 30, 0, 0),
				new TestPeriod(new DateTime(2018, 11, 1), new DateTime(2018, 11, 30), 30, 0, 30, 0),
				new TestPeriod(new DateTime(2019, 1, 1), new DateTime(2019, 1, 30), 30, 0, 0, 30),
			});
		}

		[TestMethod]
		public void Test11()
		{
			RunTest(new DateTime(2016, 9, 1), new[]
			{
				new TestPeriod(new DateTime(2018, 1, 1), new DateTime(2018, 1, 31), 30, 30, 0, 0),
				new TestPeriod(new DateTime(2018, 2, 1), new DateTime(2018, 2, 28), 30, 0, 30, 0),
				new TestPeriod(new DateTime(2019, 2, 1), new DateTime(2019, 3, 5), 35, 35, 0, 0),
			});
		}

		[TestMethod]
		public void Test12()
		{
			RunTest(new DateTime(2016, 9, 1), new[]
			{
				new TestPeriod(new DateTime(2017, 12, 1), new DateTime(2017, 12, 1), 1, 1, 0, 0),
				new TestPeriod(new DateTime(2018, 11, 1), new DateTime(2018, 11, 30), 30, 30, 0, 0),
				new TestPeriod(new DateTime(2019, 1, 1), new DateTime(2019, 1, 31), 30, 30, 0, 0),
				new TestPeriod(new DateTime(2019, 2, 1), new DateTime(2019, 2, 5), 5, 0, 5, 0),
				new TestPeriod(new DateTime(2019, 3, 1), new DateTime(2019, 4, 25), 55, 0, 55, 0),
				new TestPeriod(new DateTime(2019, 4, 26), new DateTime(2019, 5, 5), 10, 0, 0, 10),
			});
		}

		[TestMethod]
		public void Test13()
		{
			RunTest(new DateTime(2018, 9, 1), new[]
			{
				new TestPeriod(new DateTime(2019, 2, 28), new DateTime(2019, 2, 28), 3, 3, 0, 0),
			});
		}

		[TestMethod]
		public void Test14()
		{
			RunTest(new DateTime(2018, 9, 1), new[]
			{
				new TestPeriod(new DateTime(2019, 2, 27), new DateTime(2019, 2, 27), 1, 1, 0, 0),
			});
		}

		[TestMethod]
		public void Test15()
		{
			RunTest(new DateTime(2018, 9, 1), new[]
			{
				new TestPeriod(new DateTime(2019, 2, 27), new DateTime(2019, 2, 28), 4, 4, 0, 0),
			});
		}

		[TestMethod]
		public void Test16()
		{
			RunTest(new DateTime(2018, 9, 1), new[]
			{
				new TestPeriod(new DateTime(2019, 7, 31), new DateTime(2019, 8, 31), 30, 30, 0, 0),
			});
		}

		[TestMethod]
		public void Test17()
		{
			RunTest(new DateTime(2016, 9, 1), new[]
			{
				new TestPeriod(new DateTime(2017, 1, 1), new DateTime(2017, 1, 31), 30, 30, 0, 0),
				new TestPeriod(new DateTime(2017, 2, 1), new DateTime(2017, 2, 28), 30, 0, 30, 0),
				new TestPeriod(new DateTime(2018, 1, 15), new DateTime(2018, 2, 14), 30, 30, 0, 0),
				new TestPeriod(new DateTime(2018, 2, 15), new DateTime(2018, 2, 15), 1, 0, 0, 1),
			});
		}

		[TestMethod]
		public void Test17Bis()
		{
			RunTest(new DateTime(2016, 9, 1), new[]
			{
				new TestPeriod(new DateTime(2017, 1, 1), new DateTime(2017, 1, 31), 30, 30, 0, 0),
				new TestPeriod(new DateTime(2017, 2, 1), new DateTime(2017, 2, 28), 30, 0, 30, 0),
				new TestPeriod(new DateTime(2018, 1, 15), new DateTime(2018, 2, 15), 31, 30, 0, 1),
			});
		}

		[TestMethod]
		public void Test18()
		{
			RunTest(new DateTime(2016, 9, 1), new[]
			{
				new TestPeriod(new DateTime(2017, 1, 1), new DateTime(2017, 1, 15), 15, 15, 0, 0),
				new TestPeriod(new DateTime(2017, 1, 18), new DateTime(2017, 1, 31), 13, 13, 0, 0),
				new TestPeriod(new DateTime(2017, 2, 1), new DateTime(2017, 2, 2), 2, 2, 0, 0),
				new TestPeriod(new DateTime(2017, 3, 1), new DateTime(2017, 3, 31), 30, 0, 30, 0),
				new TestPeriod(new DateTime(2018, 1, 1), new DateTime(2018, 1, 15), 15, 15, 0, 0),
			});
		}

		[TestMethod]
		public void Test19()
		{
			RunTest(new DateTime(2016, 9, 1), new[]
			{
				new TestPeriod(new DateTime(2017, 1, 1), new DateTime(2017, 1, 15), 15, 15, 0, 0),
				new TestPeriod(new DateTime(2017, 1, 18), new DateTime(2017, 1, 31), 13, 13, 0, 0),
				new TestPeriod(new DateTime(2017, 2, 1), new DateTime(2017, 2, 2), 2, 2, 0, 0),
				new TestPeriod(new DateTime(2017, 3, 1), new DateTime(2017, 3, 31), 30, 0, 30, 0),
				new TestPeriod(new DateTime(2018, 1, 16), new DateTime(2018, 2, 15), 30, 30, 0, 0),
			});
		}

		[TestMethod]
		public void Test20()
		{
			RunTest(new DateTime(2016, 9, 1), new[]
			{
				new TestPeriod(new DateTime(2017, 1, 1), new DateTime(2017, 1, 31), 30, 30, 0, 0),
				new TestPeriod(new DateTime(2017, 4, 1), new DateTime(2017, 4, 30), 30, 0, 30, 0),
				new TestPeriod(new DateTime(2018, 1, 15), new DateTime(2018, 2, 14), 30, 30, 0, 0),
			});
		}

		[TestMethod]
		public void Test21()
		{
			RunTest(new DateTime(2016, 9, 1), new[]
			{
				new TestPeriod(new DateTime(2017, 1, 1), new DateTime(2017, 1, 31), 30, 30, 0, 0),
				new TestPeriod(new DateTime(2017, 4, 1), new DateTime(2017, 4, 30), 30, 0, 30, 0),
				new TestPeriod(new DateTime(2018, 3, 1), new DateTime(2018, 3, 31), 30, 30, 0, 0),
			});
		}

		private void RunTest(DateTime recruitementDate, params TestPeriod[] periods)
		{
			var viewModel = new MainWindowViewModel(new TestMessageBox(), new TestDialog());

			viewModel.RecruitementDate = recruitementDate;

			foreach (var p in periods.OrderBy(n => n.StartDate))
			{
				viewModel.StartDate = p.StartDate;
				viewModel.EndDate = p.EndDate;
				Assert.IsTrue(viewModel.AddPeriodCommand.CanExecute());
				viewModel.AddPeriodCommand.Execute();
			}

			Assert.AreEqual(periods.Length, viewModel.Periods.Count);

			for (var i = 0; i < periods.Length; i++)
			{
				var testPeriod = periods.OrderBy(n => n.StartDate).ElementAt(i);
				var actualPeriod = viewModel.Periods.OrderBy(n => n.StartDate).ElementAt(i);

				Assert.AreEqual(testPeriod.ExpectedAccountingDays, actualPeriod.AccountingDays, "erreur jours comptables");
				Assert.AreEqual(testPeriod.ExpectedFull, actualPeriod.Full, "erreur PT");
				Assert.AreEqual(testPeriod.ExpectedHalf, actualPeriod.Half, "erreur DT");
				Assert.AreEqual(testPeriod.ExpectedNone, actualPeriod.None, "erreur ST");
			}
		}
	}

	public class TestPeriod
	{
		public DateTime StartDate { get; }
		public DateTime EndDate { get; }
		public int ExpectedAccountingDays { get; }
		public int ExpectedFull { get; }
		public int ExpectedHalf { get; }
		public int ExpectedNone { get; }

		public TestPeriod(DateTime startDate, DateTime endDate, int expectedAccountingDays, int expectedFull, int expectedHalf, int expectedNone)
		{
			StartDate = startDate;
			EndDate = endDate;
			ExpectedAccountingDays = expectedAccountingDays;
			ExpectedFull = expectedFull;
			ExpectedHalf = expectedHalf;
			ExpectedNone = expectedNone;
		}
	}
}
