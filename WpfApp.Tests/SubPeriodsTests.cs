﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Collections.Generic;
using WpfApp.UI;

namespace WpfApp.Tests
{
	[TestClass]
	public class SubPeriodsTests
	{
		[TestMethod]
		public void SubPeriodTests()
		{
			for (var recruitementYearIndex = 0; recruitementYearIndex < 4; recruitementYearIndex++)
			{
				for (var periodStartDayIndex = 0; periodStartDayIndex < 500; periodStartDayIndex++)
				{
					for (var periodEndDayIndex = 0; periodEndDayIndex < 500; periodEndDayIndex++)
					{
						var startDate = new DateTime(2018, 1, 1).AddDays(periodStartDayIndex);
						var endDate = new DateTime(2018, 1, 1).AddDays(periodStartDayIndex + periodEndDayIndex);

						var period = new Period(startDate, endDate);
						period.CalculateDays(new DateTime(2012, 9, 1).AddYears(recruitementYearIndex), new List<Period> { period });

						if (period.SubPeriods.All(n => !(n.EndDate.Month == 2 && n.EndDate.Day == 27)) && period.HasErrors)
							Assert.Fail();

						for (var i = 0; i < period.SubPeriods.Count - 1; i++)
						{
							var subPeriodEndDate = period.SubPeriods[i].EndDate.AddDays(1);
							Assert.AreEqual(subPeriodEndDate.Year, period.SubPeriods[i + 1].StartDate.Year);
							Assert.AreEqual(subPeriodEndDate.Month, period.SubPeriods[i + 1].StartDate.Month);
							Assert.AreEqual(subPeriodEndDate.Day, period.SubPeriods[i + 1].StartDate.Day);
						}
					}
				}
			}
		}
	}
}