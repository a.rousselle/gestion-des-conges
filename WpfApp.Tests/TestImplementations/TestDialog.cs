﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using WpfApp.UI.Interfaces;

namespace WpfApp.Tests.TestImplementations
{
	public class TestDialog : IDialog
	{
		public void OpenFileDialog(string filter, bool multiselect, Action<OpenFileDialogResult> callback = null)
		{
			Assert.Fail();
		}

		public void SaveFileDialog(string filter, Action<SaveFileDialogResult> callback = null)
		{
			Assert.Fail();
		}
	}
}
