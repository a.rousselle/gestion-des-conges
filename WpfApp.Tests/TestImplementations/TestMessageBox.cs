﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using WpfApp.UI.Interfaces;

namespace WpfApp.Tests.TestImplementations
{
	public class TestMessageBox : IMessageBox
	{
		public void Ask(string text, string title, Icons icon, Action<AskMessageBoxResult> callback = null)
		{
			Assert.Fail();
		}

		public void Show(string text, string title, Icons icon, Action callback = null)
		{
			Assert.Fail();
		}
	}
}
