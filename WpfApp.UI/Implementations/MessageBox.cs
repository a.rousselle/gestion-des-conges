﻿using System;
using System.Windows;
using WpfApp.UI.Interfaces;

namespace WpfApp.UI.Implementations
{
	public class MessageBox : IMessageBox
	{
		public void Ask(string text, string title, Icons icon, Action<AskMessageBoxResult> callback = null)
		{
			var res = System.Windows.MessageBox.Show(text,
				title,
				MessageBoxButton.YesNo,
				GetImage(icon));

			callback?.Invoke(
				new AskMessageBoxResult{
					IsConfirmed = res == MessageBoxResult.Yes
				});
		}

		public void Show(string text, string title, Icons icon, Action callback = null)
		{
			System.Windows.MessageBox.Show(text,
				title,
				MessageBoxButton.OK,
				GetImage(icon));

			callback?.Invoke();
		}

		private MessageBoxImage GetImage(Icons icon)
		{
			switch (icon)
			{
				case Icons.Information:
					return MessageBoxImage.Information;
				case Icons.Error:
					return MessageBoxImage.Error;
				case Icons.Warning:
					return MessageBoxImage.Warning;
				case Icons.Question:
					return MessageBoxImage.Question;
				default:
					return MessageBoxImage.None;
			}
		}
	}
}
