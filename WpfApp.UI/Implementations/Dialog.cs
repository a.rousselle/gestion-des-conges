﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApp.UI.Interfaces;

namespace WpfApp.UI.Implementations
{
	public class Dialog : IDialog
	{
		public void OpenFileDialog(string filter, bool multiselect, Action<OpenFileDialogResult> callback = null)
		{
			var openFileDialog = new OpenFileDialog
			{
				Filter = filter,
				Multiselect = multiselect
			};

			var isConfirmed = openFileDialog.ShowDialog();

			var res = new OpenFileDialogResult();

			res.IsConfirmed = isConfirmed.HasValue && isConfirmed.Value;

			if (res.IsConfirmed)
				res.FileName = openFileDialog.FileName;

			callback?.Invoke(res);
		}

		public void SaveFileDialog(string filter, Action<SaveFileDialogResult> callback = null)
		{
			var saveFileDialog = new SaveFileDialog
			{
				Filter = filter
			};

			var isConfirmed = saveFileDialog.ShowDialog();

			var res = new SaveFileDialogResult();

			res.IsConfirmed = isConfirmed.HasValue && isConfirmed.Value;

			if (res.IsConfirmed)
				res.FileName = saveFileDialog.FileName;

			callback?.Invoke(res);
		}
	}
}
