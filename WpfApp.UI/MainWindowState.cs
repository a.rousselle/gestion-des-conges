﻿using System;
using System.Collections.Generic;

namespace WpfApp.UI
{
	public class MainWindowState
	{
		public int Version { get; set; } = 1;
		public DateTime? RecruitementDate { get; set; }
		public List<PeriodState> Periods { get; set; } = new List<PeriodState>();
	}

	public class PeriodState
	{
		public DateTime StartDate { get; set; }
		public DateTime EndDate { get; set; }
	}
}
