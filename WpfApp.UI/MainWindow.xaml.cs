﻿using System;
using System.Windows;

namespace WpfApp.UI
{
	public partial class MainWindow : Window
	{
		private MainWindowViewModel _viewModel;

		public MainWindow()
		{
			_viewModel = new MainWindowViewModel(
				new Implementations.MessageBox(),
				new Implementations.Dialog());

			DataContext = _viewModel;

			Loaded += (sender, e) =>
			{
				var args = Environment.GetCommandLineArgs();
				if (args.Length > 1)
					_viewModel.LoadFile(args[1]);
			};

			InitializeComponent();
		}
	}
}
