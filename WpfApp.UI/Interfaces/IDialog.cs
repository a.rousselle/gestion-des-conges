﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp.UI.Interfaces
{
	public interface IDialog
	{
		void OpenFileDialog(string filter, bool multiselect, Action<OpenFileDialogResult> callback = null);
		void SaveFileDialog(string filter, Action<SaveFileDialogResult> callback = null);
	}

	public class OpenFileDialogResult
	{
		public bool IsConfirmed { get; set; }
		public string FileName { get; set; }
	}

	public class SaveFileDialogResult
	{
		public bool IsConfirmed { get; set; }
		public string FileName { get; set; }
	}
}
