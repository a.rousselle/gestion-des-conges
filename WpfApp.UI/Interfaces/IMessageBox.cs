﻿using System;

namespace WpfApp.UI.Interfaces
{
	public enum Icons
	{
		Information,
		Question,
		Warning,
		Error
	}
	public interface IMessageBox
	{
		void Show(string text, string title, Icons icon, Action callback = null);
		void Ask(string text, string title, Icons icon, Action<AskMessageBoxResult> callback = null);
	}
	public class AskMessageBoxResult
	{
		public bool IsConfirmed { get; set; }
	}
}
