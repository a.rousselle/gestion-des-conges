﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Globalization;
using System.Linq;
using System.Collections.ObjectModel;
using System.Windows.Data;
using System.ComponentModel;
using System.IO;
using Newtonsoft.Json;
using System.Runtime.CompilerServices;
using System.Collections.Specialized;
using WpfApp.UI.Interfaces;
using System.Collections.Generic;

namespace WpfApp.UI
{
	public class MainWindowViewModel : BindableBase
	{
		#region Dependencies

		private readonly IMessageBox _messageBox;
		private readonly IDialog _dialog;

		#endregion

		public MainWindowViewModel(IMessageBox messageBox, IDialog dialog)
		{
			_messageBox = messageBox;
			_dialog = dialog;

			_periods.CollectionChanged += OnPeriodsCollectionChanged;

			Periods = new ReadOnlyObservableCollection<Period>(_periods);

			var periodsCollectionView = CollectionViewSource.GetDefaultView(Periods);
			periodsCollectionView.SortDescriptions.Add(new SortDescription("StartDate", ListSortDirection.Ascending));
			periodsCollectionView.SortDescriptions.Add(new SortDescription("EndDate", ListSortDirection.Ascending));
		}

		#region Private fields

		private FileInfo _selectedFile = null;
		private DateTime? _recruitementDate;
		private DateTime? _startDate;
		private DateTime? _endDate;
		private DelegateCommand _addPeriodCommand;
		private DelegateCommand _deletePeriodCommand;
		private DelegateCommand _newFileCommand;
		private DelegateCommand _openFileCommand;
		private DelegateCommand _saveFileCommand;
		private DelegateCommand _saveFileAsCommand;
		private ObservableCollection<Period> _periods = new ObservableCollection<Period>();

		#endregion

		#region Public properties

		public string SelectedFile
		{
			get => _selectedFile != null ? _selectedFile.Name.Replace(_selectedFile.Extension, "") : "[Nouveau]";
			set
			{
				_selectedFile = (value != null ? new FileInfo(value) : null);
				RaisePropertyChanged();
			}
		}

		public DateTime? RecruitementDate
		{
			get => _recruitementDate;
			set
			{
				SetProperty(ref _recruitementDate, value);
				RefreshFields();
			}
		}

		public DateTime? StartDate
		{
			get => _startDate;
			set
			{
				SetProperty(ref _startDate, value);
				AddPeriodCommand.RaiseCanExecuteChanged();
			}
		}

		public DateTime? EndDate
		{
			get => _endDate;
			set
			{
				SetProperty(ref _endDate, value);
				AddPeriodCommand.RaiseCanExecuteChanged();
			}
		}

		public DelegateCommand AddPeriodCommand
		{
			get => _addPeriodCommand ??
				(_addPeriodCommand = new DelegateCommand(
					(() =>
					{
						var startDate = _startDate.Value.Date;
						var endDate = _endDate.Value.Date;

						var otherPeriod = Periods.FirstOrDefault(n =>
							(n.StartDate <= startDate && startDate <= n.EndDate)
							|| (n.StartDate <= endDate && endDate <= n.EndDate)
							|| (startDate <= n.StartDate && n.EndDate <= endDate));

						if (otherPeriod != null)
						{
							_messageBox.Show("Les dates que vous avez saisies se chevauchent avec les congés déjà entrés.",
								"Attention",
								Icons.Warning);
							return;
						}

						_periods.Add(new Period(startDate, endDate));
					}),
					() => _startDate.HasValue && _endDate.HasValue && _startDate.Value.Date <= _endDate.Value.Date));
		}

		//public DelegateCommand DeletePeriodCommand
		//{
		//	get => _deletePeriodCommand ??
		//		(_deletePeriodCommand = new DelegateCommand(
		//			() => _periods.Remove(_periodsCollectionView.CurrentItem as Period),
		//			() => _periodsCollectionView.CurrentItem != null));
		//}

		public DelegateCommand DeletePeriodCommand
		{
			get => _deletePeriodCommand ??
				(_deletePeriodCommand = new DelegateCommand(
					() => _periods.Where(n => n.IsSelected).ToList().ForEach(n => _periods.Remove(n))));
		}

		public DelegateCommand NewFileCommand
		{
			get => _newFileCommand ??
				(_newFileCommand = new DelegateCommand(
					() =>
					{
						SelectedFile = null;
						RecruitementDate = null;
						_periods.Clear();
					}));
		}

		public DelegateCommand OpenFileCommand
		{
			get => _openFileCommand ??
				(_openFileCommand = new DelegateCommand(
					() =>
					{
						_dialog.OpenFileDialog(
							"Fichier cgs (*.cgs)|*.cgs",
							false,
							res =>
							{
								if (!res.IsConfirmed)
									return;
								LoadFile(res.FileName);
							});
					}));
		}

		public DelegateCommand SaveFileCommand
		{
			get => _saveFileCommand ??
				(_saveFileCommand = new DelegateCommand(
					() =>
					{
						if (_selectedFile == null)
							SaveFileAsCommand.Execute();
						else
						{
							var content = JsonConvert.SerializeObject(GetState());

							try
							{
								using (var streamWriter = new StreamWriter(_selectedFile.FullName, false))
								{
									streamWriter.Write(content);
									streamWriter.Flush();
								}
							}
							catch
							{
								_messageBox.Show("Erreur lors de l'écriture du fichier.",
									"Erreur",
									Icons.Error);
								return;
							}
						}
					}));
		}

		public DelegateCommand SaveFileAsCommand
		{
			get => _saveFileAsCommand ??
				(_saveFileAsCommand = new DelegateCommand(
					() =>
					{
						_dialog.SaveFileDialog(
							"Fichier cgs (*.cgs)|*.cgs",
							res =>
							{
								if (!res.IsConfirmed)
									return;
								SelectedFile = res.FileName;
								SaveFileCommand.Execute();
							});
					}));
		}

		public ReadOnlyObservableCollection<Period> Periods { get; }

		#endregion

		#region Public methods

		public void LoadFile(string filename)
		{
			var fileInfo = new FileInfo(filename);

			if (!fileInfo.Exists)
			{
				_messageBox.Show("Le fichier spécifié n'existe pas.",
					"Attention",
					Icons.Warning);
				return;
			}

			SelectedFile = filename;

			string content;

			using (var stream = fileInfo.OpenRead())
			{
				using (var streamReader = new StreamReader(stream))
				{
					content = streamReader.ReadToEnd();
				}
			}

			MainWindowState state = null;

			try
			{
				state = JsonConvert.DeserializeObject<MainWindowState>(content);
			}
			catch
			{
				_messageBox.Show("Erreur lors de la lecture du fichier.",
					"Erreur",
					Icons.Error);
				return;
			}

			LoadState(state);
		}

		#endregion

		#region Private methods

		private MainWindowState GetState()
		{
			return new MainWindowState
			{
				RecruitementDate = RecruitementDate,
				Periods = Periods.Select(n => new PeriodState
				{
					StartDate = n.StartDate,
					EndDate = n.EndDate
				}).ToList()
			};
		}

		private void LoadState(MainWindowState state)
		{
			_periods.CollectionChanged -= OnPeriodsCollectionChanged;
			_periods.Clear();
			state.Periods.ForEach(n => _periods.Add(new Period(n.StartDate, n.EndDate)));
			_periods.CollectionChanged += OnPeriodsCollectionChanged;
			RecruitementDate = state.RecruitementDate;
		}

		private void OnPeriodsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
		{
			RefreshFields();
		}

		private void RefreshFields()
		{
			foreach (var period in Periods)
				period.Reset();

			if (!RecruitementDate.HasValue)
				return;

			foreach (var currentPeriod in Periods.OrderBy(n => n.StartDate))
			{
				currentPeriod.CalculateDays(RecruitementDate.Value, Periods.ToList());
			}
		}

		#endregion
	}

	public static class DateTimeExtension
	{
		public const int MonthLength = 30;
		public static CultureInfo Culture = CultureInfo.GetCultureInfo("fr-FR");
		public static string ShortDateFormat = "dd/MM/yyyy";

		public static DateTime GetCalculatedStartDate(this DateTime startDate, IEnumerable<Period> periods)
		{
			var calculatedStartDate = startDate.AddDays(-1).Date;

			while (calculatedStartDate.DayOfWeek == DayOfWeek.Saturday || calculatedStartDate.DayOfWeek == DayOfWeek.Sunday)
				calculatedStartDate = calculatedStartDate.AddDays(-1);

			var previousPeriod = periods.FirstOrDefault(n => n.EndDate.Year == calculatedStartDate.Year
				&& n.EndDate.Month == calculatedStartDate.Month
				&& n.EndDate.Day == calculatedStartDate.Day);

			return previousPeriod == null
				? startDate
				: GetCalculatedStartDate(previousPeriod.StartDate, periods);
		}

		public static int GetAccountingDays(this DateTime d1, DateTime d2)
		{
			if (d2.Date > d1.Date) return -1;

			var totalDays1 = (d1.Year - 1) * 12 * MonthLength + (d1.Month - 1) * MonthLength;
			var totalDays2 = (d2.Year - 1) * 12 * MonthLength + (d2.Month - 1) * MonthLength + d2.Day;

			totalDays1 += (d1.Day >= MonthLength || (d1.Day >= 28 && d1.Month == 2)) ? MonthLength : d1.Day;

			return 1 + totalDays1 - totalDays2;
		}

		public static void GetRights(this DateTime referenceDate, DateTime? recruitementDate, out int totalDaysFull, out int totalDaysHalf)
		{
			if (!recruitementDate.HasValue || referenceDate < recruitementDate.Value)
			{
				totalDaysFull = 0;
				totalDaysHalf = 0;
				return;
			}

			var months = referenceDate.GetAccountingDays(recruitementDate.Value) / MonthLength;

			if (months < 4)
			{
				totalDaysFull = 0;
				totalDaysHalf = 0;
				return;
			}
			if (months < 24)
			{
				totalDaysFull = 30;
				totalDaysHalf = 30;
				return;
			}
			if (months < 36)
			{
				totalDaysFull = 60;
				totalDaysHalf = 60;
				return;
			}
			totalDaysFull = 90;
			totalDaysHalf = 90;
		}
	}

	public class Period : Tuple<DateTime, DateTime>, INotifyPropertyChanged
	{
		private bool _isSelected;
		private int _full;
		private int _half;
		private int _none;
		private bool _hasError;
		private ObservableCollection<SubPeriod> _subPeriods = new ObservableCollection<SubPeriod>();

		public Period(DateTime startDate, DateTime endDate) : base(startDate, endDate)
		{
			Days = (endDate - startDate).Days + 1;
			AccountingDays = endDate.GetAccountingDays(startDate);
			SubPeriods = new ReadOnlyObservableCollection<SubPeriod>(_subPeriods);
		}

		public DateTime StartDate => Item1;
		public DateTime EndDate => Item2;
		public int Days { get; }
		public int AccountingDays { get; }
		public bool IsSelected
		{
			get => _isSelected;
			set
			{
				_isSelected = value;
				OnPropertyChanged();
			}
		}
		public int Full
		{
			get => _full;
			private set
			{
				_full = value;
				OnPropertyChanged();
			}
		}
		public int Half
		{
			get => _half;
			private set
			{
				_half = value;
				OnPropertyChanged();
			}
		}
		public int None
		{
			get => _none;
			private set
			{
				_none = value;
				OnPropertyChanged();
			}
		}
		public bool HasErrors
		{
			get => _hasError;
			private set
			{
				_hasError = value;
				OnPropertyChanged();
			}
		}
		public ReadOnlyObservableCollection<SubPeriod> SubPeriods { get; }

		public event PropertyChangedEventHandler PropertyChanged;

		private void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		public void Reset()
		{
			HasErrors = false;
			Full = 0;
			Half = 0;
			None = 0;
		}

		private void RefreshSubPeriods()
		{
			_subPeriods.Clear();

			var startDate = StartDate;

			if (Full > 0)
			{
				var subPeriod = GetSubPeriod(Full, startDate, startDate, EndDate, "PT");
				_subPeriods.Add(subPeriod);
				startDate = subPeriod.EndDate.AddDays(1);
			}
			if (Half > 0)
			{
				var subPeriod = GetSubPeriod(Half, startDate, startDate, EndDate, "DT");
				_subPeriods.Add(subPeriod);
				startDate = subPeriod.EndDate.AddDays(1);
			}
			if (None > 0)
			{
				var subPeriod = GetSubPeriod(None, startDate, startDate, EndDate, "ST");
				_subPeriods.Add(subPeriod);
				startDate = subPeriod.EndDate.AddDays(1);
			}
		}

		private static SubPeriod GetSubPeriod(int accountingDays, DateTime startDate, DateTime endDate, DateTime maxEndDate, string type)
		{
			var date = endDate.AddDays(1);
			if (date.Date > maxEndDate.Date || date.GetAccountingDays(startDate) > accountingDays)
				return new SubPeriod(startDate, endDate, type);
			return GetSubPeriod(accountingDays, startDate, date, maxEndDate, type);
		}

		public void CalculateDays(DateTime recruitementDate, List<Period> allPeriods)
		{
			int totalDaysFull, totalDaysHalf;

			var calculatedStartDate = StartDate.GetCalculatedStartDate(allPeriods);

			calculatedStartDate.GetRights(recruitementDate, out totalDaysFull, out totalDaysHalf);

			var daysFunc = new Func<DateTime, DateTime>((date) => date.AddYears(-1));

			#region 365 ou 300 jours

			var previousPeriods = allPeriods
				.Where(n => n.StartDate.Date >= calculatedStartDate.AddYears(-1).Date && n.EndDate.Date < calculatedStartDate.Date)
				.OrderBy(n => n.StartDate)
				.ToList();

			if (previousPeriods.Count > 0)
			{
				//on regarde si il n'y a qu'une période de congés continue pendant les 365 jours
				var calculatedStartDateBis = previousPeriods[previousPeriods.Count - 1].StartDate.GetCalculatedStartDate(previousPeriods);
				if (!(calculatedStartDateBis.Year == previousPeriods[0].StartDate.Year
					&& calculatedStartDateBis.Month == previousPeriods[0].StartDate.Month
					&& calculatedStartDateBis.Day == previousPeriods[0].StartDate.Day))
				{
					// si il y a plusieurs périodes non continues, on prend 300 jours
					daysFunc = new Func<DateTime, DateTime>((date) => date.AddDays(-300));
				}
			}

			#endregion

			var otherPeriods = allPeriods
				.Where(n => n.StartDate.Date >= daysFunc(calculatedStartDate).Date && n.EndDate.Date < StartDate.Date)
				.OrderBy(n => n.StartDate)
				.ToList();

			foreach (var otherPeriod in otherPeriods)
			{
				totalDaysFull -= otherPeriod.Full;
				totalDaysHalf -= otherPeriod.Half;
			}

			var rest = AccountingDays;

			Full = totalDaysFull >= rest ? rest : totalDaysFull;
			rest -= Full;
			Half = totalDaysHalf >= rest ? rest : totalDaysHalf;
			rest -= Half;
			None = rest;

			RefreshSubPeriods();

			var s = SubPeriods.FirstOrDefault(n => n.Type == "PT");
			if (Full > 0 && (s == null || s.AccountingDays != Full))
			{
				HasErrors = true;
				return;
			}

			s = SubPeriods.FirstOrDefault(n => n.Type == "DT");
			if (Half > 0 && (s == null || s.AccountingDays != Half))
			{
				HasErrors = true;
				return;
			}

			s = SubPeriods.FirstOrDefault(n => n.Type == "ST");
			if (None > 0 && (s == null || s.AccountingDays != None))
			{
				HasErrors = true;
				return;
			}
		}
	}

	public class SubPeriod : Tuple<DateTime, DateTime>
	{
		public SubPeriod(DateTime startDate, DateTime endDate, string type) : base(startDate, endDate)
		{
			AccountingDays = endDate.GetAccountingDays(startDate);
			Type = type;
		}
		public DateTime StartDate => Item1;
		public DateTime EndDate => Item2;
		public int AccountingDays { get; }
		public string Type { get; }
	}
}
